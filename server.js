var express = require('express');
var app = express();
var path = require('path');

app.set('views', __dirname + '/views/main');
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));
//app.listen( 8558, "192.168.1.7");
app.listen(process.env.port || 8080);

require('./features/link_redirects.js').redirectUrls(app);
app.get('/', function(req, res) {
	res.render('index');
});
