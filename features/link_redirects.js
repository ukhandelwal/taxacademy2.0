exports.redirectUrls = function (app) {
    app.get('/js/:id', function (req, res) {
        res.sendFile(req.params.id, { root: __dirname + '/../views/js' });
    });

        app.get('/font-awesome/css/:id', function (req, res) {
        res.sendFile(req.params.id, { root: __dirname + '/../views/font-awesome/css' });
    });

    app.get('/font-awesome/fonts/:id', function (req, res) {
        console.log(req.params.id);
        res.sendFile(req.params.id, { root: __dirname + '/../views/font-awesome/fonts' });
    });

    app.get('/font-awesome/less/:id', function (req, res) {
        console.log(req.params.id);
        res.sendFile(req.params.id, { root: __dirname + '/../views/font-awesome/less' });
    });

    app.get('/font-awesome/scss/:id', function (req, res) {
        console.log(req.params.id);
        res.sendFile(req.params.id, { root: __dirname + '/../views/font-awesome/scss' });
    });

}